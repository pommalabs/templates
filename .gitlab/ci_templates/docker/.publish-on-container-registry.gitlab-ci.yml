.publish-on-container-registry:
  stage: publish
  image:
    name: docker:latest
    pull_policy: if-not-present
  services:
    - name: docker:dind
      pull_policy: if-not-present
  variables:
    DOCKERFILE_LOCATION: "Dockerfile"
    # When using dind service, you must instruct Docker
    # to talk with the daemon started inside of the service.
    # The daemon is available with a network connection
    # instead of the default `/var/run/docker.sock` socket.
    DOCKER_HOST: tcp://docker:2376
    # Specify to Docker where to create the certificates.
    # Docker creates them automatically on boot,
    # and creates `/certs/client` to share between the service
    # and job container, thanks to volume mount from config.
    DOCKER_TLS_CERTDIR: "/certs"
    # These are usually specified by the entrypoint, however the
    # Kubernetes executor doesn't run entrypoints:
    # https://gitlab.com/gitlab-org/gitlab-runner/-/issues/4125
    DOCKER_TLS_VERIFY: 1
    DOCKER_CERT_PATH: "$DOCKER_TLS_CERTDIR/client"
  cache: []
  before_script:
    - apk add --no-cache git
    - git --version
    - until docker info; do sleep 1; done
    - docker context create single-arch-context
    - docker buildx create --use single-arch-context
    - docker buildx inspect --bootstrap
  script:
    - IMAGE_TAG="$CONTAINER_REGISTRY_HOST/$CONTAINER_REGISTRY_ORGANIZATION/$CONTAINER_REGISTRY_REPOSITORY:$IMAGE_TAG"
    - docker login -u $CONTAINER_REGISTRY_USER -p $CONTAINER_REGISTRY_PASSWORD $CONTAINER_REGISTRY_HOST
    - |
      docker buildx build --sbom=true -t $IMAGE_TAG --push \
        --label "maintainer=$IMAGE_AUTHOR" \
        --label "org.opencontainers.image.authors=$IMAGE_AUTHOR" \
        --label "org.opencontainers.image.created=$(date -Iseconds)" \
        --label "org.opencontainers.image.url=$CI_PROJECT_URL" \
        --label "org.opencontainers.image.documentation=$CI_PROJECT_URL" \
        --label "org.opencontainers.image.source=$CI_PROJECT_URL" \
        --label "org.opencontainers.image.revision=$CI_COMMIT_SHA" \
        --label "org.opencontainers.image.vendor=$IMAGE_VENDOR" \
        --label "org.opencontainers.image.title=$IMAGE_TITLE" \
        --label "description=$IMAGE_DESCRIPTION" \
        --label "org.opencontainers.image.description=$IMAGE_DESCRIPTION" \
        -f $DOCKERFILE_LOCATION .
    - docker buildx imagetools inspect $IMAGE_TAG
  # This task relies on a container registry, which might have temporary failures.
  retry: 1

.publish-on-container-registry-multi-arch:
  stage: publish
  image:
    name: docker:latest
    pull_policy: if-not-present
  services:
    - name: docker:dind
      pull_policy: if-not-present
      command: ["--experimental"]
  variables:
    DOCKERFILE_LOCATION: "Dockerfile"
    # When using dind service, you must instruct Docker
    # to talk with the daemon started inside of the service.
    # The daemon is available with a network connection
    # instead of the default `/var/run/docker.sock` socket.
    DOCKER_HOST: tcp://docker:2376
    # Specify to Docker where to create the certificates.
    # Docker creates them automatically on boot,
    # and creates `/certs/client` to share between the service
    # and job container, thanks to volume mount from config.
    DOCKER_TLS_CERTDIR: "/certs"
    # These are usually specified by the entrypoint, however the
    # Kubernetes executor doesn't run entrypoints:
    # https://gitlab.com/gitlab-org/gitlab-runner/-/issues/4125
    DOCKER_TLS_VERIFY: 1
    DOCKER_CERT_PATH: "$DOCKER_TLS_CERTDIR/client"
  cache: []
  before_script:
    - apk add --no-cache git
    - git --version
    - until docker info; do sleep 1; done
    - docker run --privileged --rm tonistiigi/binfmt --uninstall arm64
    - docker run --privileged --rm tonistiigi/binfmt --install arm64
    - docker context create multi-arch-context
    - docker buildx create --use multi-arch-context
    - docker buildx inspect --bootstrap
  script:
    - IMAGE_TAG="$CONTAINER_REGISTRY_HOST/$CONTAINER_REGISTRY_ORGANIZATION/$CONTAINER_REGISTRY_REPOSITORY:$IMAGE_TAG"
    - docker login -u $CONTAINER_REGISTRY_USER -p $CONTAINER_REGISTRY_PASSWORD $CONTAINER_REGISTRY_HOST
    - |
      docker buildx build --sbom=true -t $IMAGE_TAG --push \
        --label "maintainer=$IMAGE_AUTHOR" \
        --label "org.opencontainers.image.authors=$IMAGE_AUTHOR" \
        --label "org.opencontainers.image.created=$(date -Iseconds)" \
        --label "org.opencontainers.image.url=$CI_PROJECT_URL" \
        --label "org.opencontainers.image.documentation=$CI_PROJECT_URL" \
        --label "org.opencontainers.image.source=$CI_PROJECT_URL" \
        --label "org.opencontainers.image.revision=$CI_COMMIT_SHA" \
        --label "org.opencontainers.image.vendor=$IMAGE_VENDOR" \
        --label "org.opencontainers.image.title=$IMAGE_TITLE" \
        --label "description=$IMAGE_DESCRIPTION" \
        --label "org.opencontainers.image.description=$IMAGE_DESCRIPTION" \
        --platform "linux/amd64,linux/arm64" \
        -f $DOCKERFILE_LOCATION .
    - docker buildx imagetools inspect $IMAGE_TAG
  # This task relies on a container registry, which might have temporary failures.
  # Moreover, ARM64 builds sometimes get stuck on GitLab CI and they make the pipeline
  # fail by reaching the maximum timeout. Retrying the build usually solves the issue.
  retry: 1
